# Ismena OS

> This project is a small hobby os development in OCaml and ATS. The starting point of this project is [https://github.com/rizo/snowflake-os/tree/7818e9441a925717664e1c5caa0f3a070189e1d9](https://github.com/rizo/snowflake-os/tree/7818e9441a925717664e1c5caa0f3a070189e1d9) which is called Snowflake-os in OCaml.
